import 'package:movieflix/models/movie_details_model.dart';
import 'package:movieflix/models/movie_model.dart';
import 'package:movieflix/services/networking.dart';

const apiKey = 'd023c237ec71d6a50e32e635042fb18f';
const openMovieURL = 'https://api.themoviedb.org/3';

class RequestHelper {
  Future<List<Movie>> searchMovie(String movie) async {
    List<Movie> movies = [];
    try {
      NetworkHelper networkHelper = NetworkHelper(
          '$openMovieURL/search/movie?api_key=d023c237ec71d6a50e32e635042fb18f&language=en-US&query=$movie&page=1&include_adult=false');

      var moviesData = await networkHelper.getData();
      Map<String, dynamic> response = moviesData;
      List<dynamic> result = response["results"];
      result.forEach((element) => movies.add(Movie.fromJson(element)));
    } catch (e) {
      print(e);
    }
    return movies;
  }

  Future<List<Movie>> getTopMovies() async {
    List<Movie> movies = [];

    NetworkHelper networkHelper = NetworkHelper(
        '$openMovieURL/movie/popular?api_key=d023c237ec71d6a50e32e635042fb18f&language=en-US&page=1');

    var moviesData = await networkHelper.getData();
    Map<String, dynamic> response = moviesData;
    List<dynamic> result = response["results"];
    result.forEach((element) => movies.add(Movie.fromJson(element)));
    return movies;
  }

  Future<MovieDetailsModel> getMovie(String id) async {
    MovieDetailsModel movie = MovieDetailsModel();
    NetworkHelper networkHelper =
        NetworkHelper('$openMovieURL/movie/$id?api_key=$apiKey&language=en-US');
    var moviesData = await networkHelper.getData();
    Map<String, dynamic> response = moviesData;
    movie = MovieDetailsModel.fromJson(response);
    print(movie.title);
    return movie;
    //print(result);
  }
}
