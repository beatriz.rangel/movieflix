import 'package:flutter/material.dart';
import 'package:movieflix/screens/home_page.dart';
import 'package:movieflix/screens/movie_details.dart';
import 'package:movieflix/screens/search_movies.dart';

void main() {
  runApp(const MovieFlix());
}

class MovieFlix extends StatelessWidget {
  const MovieFlix({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
          backgroundColor: Colors.black,
          scaffoldBackgroundColor: Colors.black,
          brightness: Brightness.dark,
          primaryColor: Colors.black),
      home: const SearchMoviesPage(),
    );
  }
}
