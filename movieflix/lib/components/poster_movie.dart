import 'package:flutter/material.dart';

class PosterMovie extends StatelessWidget {
  const PosterMovie({
    required this.id,
    required this.poster,
  });

  final id;
  final String? poster;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      width: MediaQuery.of(context).size.width * 0.5,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(
              'https://image.tmdb.org/t/p/original/$poster',
            ),
            fit: BoxFit.cover),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      margin: const EdgeInsets.all(15.0),
    );
  }
}
