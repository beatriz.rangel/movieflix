import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SuperPosterWidget extends StatelessWidget {
  const SuperPosterWidget({
    Key? key,
    required this.posterPath,
    required this.ranking,
    required this.releaseDate,
  }) : super(key: key);

  final String? posterPath;
  final double? ranking;
  final String? releaseDate;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          //height: MediaQuery.of(context).size.height * 0.75,
          height: 500,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    'https://image.tmdb.org/t/p/original$posterPath'),
                fit: BoxFit.cover),
          ),
        ),
        Container(
          height: 500,
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
                colors: [Colors.black.withOpacity(0.85), Colors.transparent],
                end: Alignment.topCenter,
                begin: Alignment.bottomCenter),
          ),
        ),
        const Positioned(top: 20, left: 5, child: Icon(Icons.arrow_back)),
        Positioned(
            bottom: 20,
            left: 15,
            child: Row(
              children: [
                const Icon(
                  Icons.star,
                  color: Colors.yellow,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text('Rankings: $ranking'),
                const SizedBox(
                  width: 30,
                ),
                Text('Released Date: $releaseDate')
              ],
            ))
      ],
    );
  }
}
