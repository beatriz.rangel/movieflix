import 'package:flutter/material.dart';
import 'package:movieflix/components/poster_movie.dart';
import 'package:movieflix/models/movie_model.dart';
import 'package:movieflix/screens/movie_details.dart';
import 'package:movieflix/services/request_helper.dart';

class SearchMoviesPage extends StatefulWidget {
  const SearchMoviesPage({Key? key}) : super(key: key);

  @override
  State<SearchMoviesPage> createState() => _SearchMoviesPageState();
}

class _SearchMoviesPageState extends State<SearchMoviesPage> {
  List<Movie> movies = [];

  Future<List<Movie>> fetchMoviesData(String movie) async {
    RequestHelper requestHelper = RequestHelper();

    movies = await requestHelper.searchMovie(movie);

    return movies;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String? itemSearch;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: Icon(Icons.arrow_back),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                prefixIcon: const Icon(Icons.search),
                hintText: 'Search',
              ),
              onChanged: (value) {
                itemSearch = value;
              },
            ),
          ),
          Center(
            child: TextButton(
              style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  padding: const EdgeInsets.only(right: 50, left: 50)),
              child: const Text(
                'Search',
                style: TextStyle(fontSize: 30, color: Colors.white),
              ),
              onPressed: () async {
                //List<Movie> result = await RequestHelper().getTopMovies();
                print("testando $itemSearch");
                setState(() {
                  fetchMoviesData(itemSearch!);
                });
              },
            ),
          ),
          Container(
            height: 300, //set size also with mediaquery
            width: size.width,
            child: ListView.builder(
              itemCount: movies.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MovieDetailsPage(
                            id: movies[index].id!,
                          );
                        },
                      ),
                    );
                  },
                  child: PosterMovie(
                      id: movies[index].id, poster: movies[index].posterPath),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
