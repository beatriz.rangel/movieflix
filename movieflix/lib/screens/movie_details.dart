import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:movieflix/components/super_poster.dart';
import 'package:movieflix/components/synopis_widget.dart';
import 'package:movieflix/components/title_widget.dart';
import 'package:movieflix/models/movie_details_model.dart';
import 'package:movieflix/services/request_helper.dart';

class MovieDetailsPage extends StatefulWidget {
  const MovieDetailsPage({Key? key, required this.id}) : super(key: key);
  final int id;
  @override
  State<MovieDetailsPage> createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  MovieDetailsModel movie = MovieDetailsModel();

  Future<MovieDetailsModel> fetchMovieDetail(String id) async {
    movie = await RequestHelper().getMovie(id);
    return movie;
  }

  String dateFormater(DateTime date) {
    var formatter = DateFormat('dd-MM-yyyy');
    String formattedDate = formatter.format(date);
    return formattedDate;
  }

  @override
  void initState() {
    fetchMovieDetail(widget.id.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 8,
            child: FutureBuilder<MovieDetailsModel>(
              future: fetchMovieDetail(widget.id.toString()),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData) {
                    String date = dateFormater(movie.releaseDate!);
                    return SuperPosterWidget(
                      ranking: movie.voteAverage,
                      releaseDate: date,
                      posterPath: movie.posterPath,
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(), //Trocar
                    );
                  }
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
          FutureBuilder<MovieDetailsModel>(
              future: fetchMovieDetail(widget.id.toString()),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return TitleWidget(
                    text: movie.title,
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }),
          Expanded(
            flex: 3,
            child: FutureBuilder<MovieDetailsModel>(
              future: fetchMovieDetail(widget.id.toString()),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return SynopsisWidget(
                    text: movie.overview,
                  );
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
