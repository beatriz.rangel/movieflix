import 'package:flutter/material.dart';
import 'package:movieflix/components/poster_movie.dart';
import 'package:movieflix/models/movie_model.dart';
import 'package:movieflix/screens/movie_details.dart';
import 'package:movieflix/services/request_helper.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Movie> movies = [];

  Future<List<Movie>> fetchPopularMovies() async {
    movies = await RequestHelper().getTopMovies();
    return movies;
  }

  @override
  void initState() {
    fetchPopularMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print("Lista no momento" + movies.length.toString());
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Favoritos da Semana",
              style: TextStyle(fontSize: 25),
            ),
            Container(
              height: 300, //set size also with mediaquery
              width: size.width,
              child: FutureBuilder<List<Movie>>(
                future: fetchPopularMovies(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                        itemCount: movies.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) {
                                    return MovieDetailsPage(
                                      id: movies[index].id!,
                                    );
                                  },
                                ),
                              );
                            },
                            child: PosterMovie(
                                id: movies[index].id,
                                poster: movies[index].posterPath),
                          );
                        },
                      );
                    } else {
                      return const Center(
                        child: CircularProgressIndicator(), //Trocar
                      );
                    }
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
